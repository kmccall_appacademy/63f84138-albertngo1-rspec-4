class Temperature
  def initialize(options = {})
    if options[:f]
      @fahrenheit = options[:f]
    elsif options[:c]
      @celsius = options[:c]
    end
  end

  def fahrenheit
    @fahrenheit
  end

  def celsius
    @celsius
  end

  def ftoc(temperature)
    (temperature - 32) * 5 / 9
  end

  def ctof(temperature)
    temperature * 9.0/5.0 +32
  end

  def in_fahrenheit
    fahrenheit || ctof(celsius)
  end

  def in_celsius
    celsius || ftoc(fahrenheit)
  end

  def self.from_celsius(temperature)
    celsius = self.new(:c => temperature)
  end

  def self.from_fahrenheit(temperature)
    fahrenheit = self.new(:f => temperature)
  end

end

class Celsius < Temperature
  def initialize(temperature)
    @celsius = temperature
  end
end

class Fahrenheit < Temperature
  def initialize(temperature)
    @fahrenheit = temperature
  end
end
