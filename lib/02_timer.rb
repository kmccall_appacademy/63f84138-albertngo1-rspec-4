class Timer

  def initialize
    @seconds = 0
  end

  def seconds=(seconds)
    @seconds = seconds
  end
  def seconds
    @seconds
  end

  def time_string

    hours = seconds / 3600

    minutes = seconds / 60 % 60

    second = seconds % 60

    sprintf("%02d:%02d:%02d", hours, minutes, second)

  end

end
