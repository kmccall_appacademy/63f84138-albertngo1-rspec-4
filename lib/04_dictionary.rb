class Dictionary

  attr_accessor :entries

  def initialize
    @entries = {}
  end

  def keywords
    entries.keys.sort
  end

  def include?(keyword)
    keywords.include?(keyword)
  end

  def add(addition)
    if addition.is_a?(String)
      entries[addition] = nil
    elsif addition.is_a?(Hash)
      entries.merge!(addition)
    end
  end

  def find(fragment)
    entries.select do |keyword, definition|
      keyword.include?(fragment)
    end
  end

  def printable

    output = []

    entries.each do |k, d|
      output << "[#{k}] \"#{d}\""
    end

    output.sort.join("\n")

  end

end
