class Book

  def name=(name)
    @name = name
  end
  def name
    @name
  end

  def title
    little_words = %w(the a an in the and of)
    name.capitalize!
    name_split = name.split(" ")
    name_split.map! do |word|
      if little_words.include?(word)
        word
      else
        word.capitalize
      end
    end

    name_split.join(" ")

  end
end
